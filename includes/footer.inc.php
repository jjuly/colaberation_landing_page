<footer id="foot">

<section id="section1">

<div>
    <img src="/images/light_phone_icon.png" id="img1"></img>
    <img src="/images/light_email_icon.png" id="img2"></img>
    <img src="/images/light_clock_icon.png" id="img3"></img>
    
    
    <p id="call-para">Call us</p>
    <p id="phone-para">+1 (123) 456789</p>



   
    <p id="email-para">Email</p>
    <p id="emai-para" class="info">info@vr.com</p>

    <p id="office-para" class="both">Office hours</p>
    <p id="time-para" class="both">Mon-Sat 8:30-4:30</p>


    
    <h2 id="company">Company</h2>
    
    <p id="lorem">Lorem ipsum dolor sit amet</p>
    <p id="lorem">elit consectetur adipisicing</p>
    <p id="lorem">sed do eiusmod tempor</p>
    <p id="lorem">incididunt et dolore elit labore</p>

    <img src="/images/location-icon-white-png-10.png" id="img4"></img>
    <p class="footer-address">Lot 1178 Warehouse</p>
    <p class="footer-address">13 APT 304B</p>


    <h2 id="page-link">Page link</h2>
    <ul>
        <li id="page-link" class="link2">Services</li>
        <li id="page-link" class="link2">Contact</li>
        <li id="page-link" class="link2">Article</li>
        <li id="page-link" class="link2">About</li>
    </ul>
    
    <h2 id="newsletter">Newsletter</h2>
    <p id="newsletter" class="newsletter-para">Lorem ipsum dolor sit</p>
    <p id="newsletter" class="newsletter-para">amet, consectetur adipisicing</p>

    
   
        <input type="email" placeholder="Your Email Address"  class="input-box"></input>
        <img src="/images/purple_location_icon2.png" id="img5"></img>

       

       
    <div> 
    <hr id="newsletter" class="horizontal-rule">
    </div>

    
    <img src="/images/light_twitter.png" id="img6"></img>
    <img src="/images/light_google.png" id="img6"></img>
    <img src="/images/light_facebook.png" id="img6"></img>


    <p id="newsletter" class="copyright"> Copyright © 2021, ashland-theme</p>
    <p id="newsletter" class="copyright">Powered by ashland</p>




</div>








</footer>



</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<script src="scripts/master.js></script>
</html>